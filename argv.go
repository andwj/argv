// Copyright 2017 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the accompanying "LICENSE.md" file for the full text.

package argv

import (
	"fmt"
	"io"
	"os"
	"unicode"
)

type reqType int

const (
	requires_NONE reqType = iota // no values required
	requires_ONE                 // one value required
	requires_MANY                // any number of values
)

type option struct {
	short string
	long  string
	what  string
	desc  string
	reqs  reqType
	gap   int

	parser  func(val string) error
	clearer func()
}

var optionList []option

var unparsedArgs []string

// this is only needed by the testing functions.
func resetForTests() {
	optionList = nil
}

// Enabler creates an option which is merely enabled by its presence
// on the command-line.  The option cannot be followed by a value.
// We assume the default value of the variable is false, and if this
// option is found then that variable is set to true.
// Either the 'short' or 'long' option name can be "" (absent).
func Enabler(short, long string, v *bool, desc string) {
	parser := func(val string) error {
		*v = true
		return nil
	}
	opt := option{short, long, "", desc, requires_NONE, 0, parser, nil}
	optionList = append(optionList, opt)
}

// Integer creates an option that needs an integer value.
// If the value cannot be parsed as an integer then an error
// is returned from the Parse() function.
// Either the 'short' or 'long' option name can be "" (absent).
// The 'what' parameter is a short word describing the kind of
// value expected, for example: "count" or "width".
func Integer(short, long string, v *int, what string, desc string) {
	parser := func(val string) error {
		n, _ := fmt.Sscan(val, v)
		if n != 1 {
			return fmt.Errorf("bad integer value: %s", val)
		}
		return nil // OK
	}
	opt := option{short, long, what, desc, requires_ONE, 0, parser, nil}
	optionList = append(optionList, opt)
}

// Float creates an option that needs an floating-point value.
// If the value cannot be parsed as a floating point literal
// then an error is returned from the Parse() function.
// Either the 'short' or 'long' option name can be "" (absent).
// The 'what' parameter is a short word describing the kind of
// value expected, for example: "power".
func Float(short, long string, v *float64, what string, desc string) {
	parser := func(val string) error {
		n, _ := fmt.Sscan(val, v)
		if n != 1 {
			return fmt.Errorf("bad numeric value: %s", val)
		}
		return nil // OK
	}
	opt := option{short, long, what, desc, requires_ONE, 0, parser, nil}
	optionList = append(optionList, opt)
}

// Generic creates an option which needs a single value, one which
// is not an integer or floating point, such as a filename.
// The value will be assigned as a string without any further
// processing or validation.
// Either the 'short' or 'long' option name can be "" (absent).
// The 'what' parameter is a short word describing the kind of
// value expected, for example: "file" or "mode".
func Generic(short, long string, v *string, what string, desc string) {
	parser := func(val string) error {
		*v = val
		return nil
	}
	opt := option{short, long, what, desc, requires_ONE, 0, parser, nil}
	optionList = append(optionList, opt)
}

// Multi creates an option which accepts zero or more arguments.
// Everything following the option which does not look like a new
// option will be included.  Like Generic(), the arguments are stored
// as strings without any further processing.
// Either the 'short' or 'long' option name can be "" (absent).
// The 'what' parameter is a short word describing the kind of
// value expected, for example: "file".
func Multi(short, long string, v *[]string, what string, desc string) {
	parser := func(val string) error {
		*v = append(*v, val)
		return nil
	}
	clearer := func() {
		*v = make([]string, 0, 1)
	}
	opt := option{short, long, what, desc, requires_MANY, 0, parser, clearer}
	optionList = append(optionList, opt)
}

// Gap merely causes a blank line between two options when the
// options are printed by the Display() function.
func Gap() {
	last := len(optionList)

	if last > 0 {
		optionList[last-1].gap = 1
	}
}

// Unparsed returns the unparsed command-line arguments, including
// those immediately before the first option and those after the
// last option (and its values).
func Unparsed() []string {
	return unparsedArgs
}

// Display formats the options into a human-readable list and
// outputs it to the given Writer.  It returns nil on success
// or an error if writing failed for some reason.
func Display(w io.Writer) error {
	var longWidth, whatWidth int

	// determine field widths
	for _, opt := range optionList {
		if opt.long != "" {
			n := 2 + len([]rune(opt.long))
			if n > longWidth {
				longWidth = n
			}
		}

		if opt.reqs != requires_NONE {
			n := 2 + len([]rune(opt.what))
			if opt.reqs == requires_MANY {
				n += 3
			}
			if n > whatWidth {
				whatWidth = n
			}
		}
	}

	// actually print the options
	for _, opt := range optionList {
		var short, long, what string

		if opt.short != "" {
			short = "-" + opt.short
		}

		if opt.long != "" {
			long = "--" + opt.long
		}

		if opt.reqs != requires_NONE {
			what = fmt.Sprintf("<%s>", opt.what)

			if opt.reqs == requires_MANY {
				what += "..."
			}
		}

		_, err := fmt.Fprintf(w, "   %-3s %-*s %-*s   %s\n",
			short, longWidth, long, whatWidth, what, opt.desc)

		if err != nil {
			return err
		}

		for i := 0; i < opt.gap; i++ {
			_, err = fmt.Fprintln(w)
			if err != nil {
				return err
			}
		}
	}

	return nil // OK
}

func isOption(arg string) bool {
	// do not match a standalone minus sign: "-"
	if len(arg) < 2 {
		return false
	}

	if arg[0] != '-' {
		return false
	}

	// does it look like a negative number?
	runes := []rune(arg)

	if len(runes) >= 2 && unicode.IsDigit(runes[1]) {
		return false
	}

	return true
}

func findOption(name string) *option {
	runes := []rune(name)
	isShort := len(runes) == 1

	for _, opt := range optionList {
		if isShort && matchOption(name, opt.short) {
			return &opt
		}
		if !isShort && matchOption(name, opt.long) {
			return &opt
		}
	}

	return nil
}

func matchOption(a string, b string) bool {
	if a == "" || b == "" {
		return false
	}

	return a == b
}

// Parse will process the command-line arguments in os.Args and
// update any variables that were previously provided to calls
// to Enabler, Integer, Generic (etc) when matches are found.
//
// Arguments which were not parsed as options or option values
// can be examined by the Unparsed() function.  The "--" option
// causes all subsequent arguments to be placed here.
//
// This function may be called multiple times.
//
// Returns nil on success, otherwise an error result.
func Parse() error {
	unparsedArgs = make([]string, 0)

	for _, opt := range optionList {
		if opt.clearer != nil {
			opt.clearer()
		}
	}

	pos := 1
	end := len(os.Args)

	for pos < end {
		arg := os.Args[pos]

		if !isOption(arg) {
			unparsedArgs = append(unparsedArgs, arg)
			pos++
			continue
		}

		if arg == "--" {
			for pos++; pos < end; pos++ {
				unparsedArgs = append(unparsedArgs, os.Args[pos])
			}
			break
		}

		isLong := (len(arg) >= 2 && arg[1] == '-')

		if isLong {
			arg = arg[2:]
		} else {
			arg = arg[1:]
		}

		opt := findOption(arg)

		if opt == nil {
			return fmt.Errorf("unknown option: %s", os.Args[pos])
		}

		switch opt.reqs {
		case requires_NONE:
			err := opt.parser("")
			if err != nil {
				return err
			}
			pos++

		case requires_ONE:
			if pos+1 >= end {
				return fmt.Errorf("missing value for %s", os.Args[pos])
			}

			val := os.Args[pos+1]

			if isOption(val) {
				return fmt.Errorf("missing value for %s", os.Args[pos])
			}

			err := opt.parser(val)
			if err != nil {
				return err
			}
			pos += 2

		case requires_MANY:
			for pos++; pos < end; pos++ {
				val := os.Args[pos]

				if isOption(val) {
					break
				}

				err := opt.parser(val)
				if err != nil {
					return err
				}
			}

		default:
			panic("internal error: bad 'reqs' value")
		}
	}

	return nil // OK
}
