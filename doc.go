// Copyright 2017 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the accompanying "LICENSE.md" file for the full text.

/*

Package argv provides basic command-line argument parsing.

by Andrew Apted, 2017.


Parsing

This package supports options which have a short form and/or a
long form.  The short form is always a single letter, and the
long form is always two or more letters.  All options MUST begin
with at least one minus sign ('-'), and long options may be
prefixed by two minus signs ('--').  For example, a single option
definition allows the following three forms to be accepted:

	-f something.txt

	-file something.txt

	--file something.txt

Negative numbers, like "-123", are always considered to be normal
arguments and not options.  A standalone minus sign is also not
considered to be an option.  The special "--" option is detected
and everything following it is considered to be normal arguments
(i.e they are not parsed for options).

An "enabler" is an option which does not take any value, rather
it merely sets a variable to true when it is present (and the
variable remains false when it is absent).  Note that short
options CANNOT be mixed together, like "-cvf" which allowed by
the tar program.

Other options require a value, and that value MUST be separated
from the option itself by at least one space.  There is support
for options which take multiple values, and all the values must
likewise be separated by spaces.

Note that currently there is NO support for using an equals sign
('=') between an option name and its value.

Command-line arguments which are not parsed as options are
collected into a list, which can be queried by the Unparsed()
function.  This list includes all arguments following the "--"
option.


Usage

Options are defined using functions of this package, such as
Enabler(), Generic(), Integer(), etc...  These functions take
a pointer to an existing variable, which will be updated if
the option is used on the command line.

These functions also take a short name and a long name, which
are given without any leading minus signs.  Either the short
name or long name can be absent (using the empty string).
The other parameters are only used for displaying the help text.

Once the options are defined, call
    argv.Parse()
to parse the command-line arguments.  It will return an error
if something could not be parsed, e.g. an option which needs
a value but none was supplied.

The Display() function will format the options and write them
to a Writer object.  This is useful to implement a "-help"
option, as in the example below.  The Gap() function can be
used to insert a blank line between sets of options.


Example

	package main

	import (
		"fmt"
		"os"

		"gitlab.com/andwj/argv"
	)

	var (
		filename string
		count    int
		massive  bool
		help     bool
	)

	func main() {
		argv.Generic("f", "file", &filename, "filename", "the file to blobify")
		argv.Integer("c", "count", &count, "num", "how many blobs to add")
		argv.Gap()
		argv.Enabler("",  "massive", &massive, "create very large blobs")
		argv.Enabler("h", "help", &help, "show program help")

		err := argv.Parse()
		if err != nil {
			fmt.Fprintf(os.Stderr, "blobify: %s\n", err.Error())
			os.Exit(1)
		}

		if help {
			fmt.Printf("USAGE: blobify [options...] [file]\n\n")
			fmt.Printf("Available options:\n")
			argv.Display(os.Stdout)
			os.Exit(0)
		}

		// when no --file option was given, look at the unparsed args
		if filename == "" {
			unparsed := argv.Unparsed()
			if len(unparsed) == 0 {
				fmt.Fprintf(os.Stderr, "blobify: missing filename\n")
				os.Exit(1)
			}
			filename = unparsed[0]
		}

		// blobify the file.....
	}


License

Copyright (c) 2017 Andrew Apted

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*/
package argv
