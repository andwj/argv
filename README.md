
Package Argv
============

*A Go package providing basic command-line argument parsing.*

by Andrew Apted, 2017


Usage
-----

Please see the [GoDoc page](https://www.godoc.org/gitlab.com/andwj/argv).


Status
------

Version is 0.8.1

This library is working well and is fairly complete.

There are tests for each different kind of option, and they
are currently all passing.

The code documentation is decent, the package documentation
is decent and includes a small example program.


License
-------

The license is MIT-style, a permissive open source license.
See the [LICENSE.md](LICENSE.md) file for the complete text.

