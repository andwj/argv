// Copyright 2017 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the accompanying "LICENSE.md" file for the full text.

package argv

import (
	// "fmt"
	"os"
	"testing"
)

func TestBasics(t *testing.T) {
	os.Args = []string{"test.exe", "xxx", "-foo", "yyy", "--bar", "zzz"}
	resetForTests()

	var foo string
	var bar bool

	Generic("f", "foo", &foo, "str", "the foo")
	Enabler("b", "bar", &bar, "the bar")

	err := Parse()
	if err != nil {
		t.Logf("Parse() returned an error: %s", err.Error())
		t.FailNow()
	}

	if foo != "yyy" {
		t.Errorf("Did not handle -foo properly")
	}

	if bar != true {
		t.Errorf("Did not handle -bar properly")
	}

	unparsed := Unparsed()

	if len(unparsed) != 2 {
		t.Logf("Wrong number of unparsed args (expected 2, got %d)", len(unparsed))
		t.FailNow()
	}

	if unparsed[0] != "xxx" || unparsed[1] != "zzz" {
		t.Errorf("Wrong values of unparsed args: %v", unparsed)
	}
}

func TestMulti(t *testing.T) {
	os.Args = []string{"test.exe", "-f", "aa", "bb", "-b", "--", "xxx", "zzz"}
	resetForTests()

	var foo []string
	var bar []string

	Multi("f", "", &foo, "str", "list of foos")
	Multi("b", "", &bar, "str", "list of bars")

	err := Parse()
	if err != nil {
		t.Logf("Parse() returned an error: %s", err.Error())
		t.FailNow()
	}

	if len(bar) != 0 {
		t.Errorf("Wrong number of -b values (expected 0, got %d)", len(bar))
	}

	if len(foo) != 2 {
		t.Logf("Wrong number of -f values (expected 2, got %d)", len(foo))
		t.FailNow()
	}

	if foo[0] != "aa" || foo[1] != "bb" {
		t.Errorf("Wrong values for -f option: %v", foo)
	}

	unparsed := Unparsed()

	if len(unparsed) != 2 {
		t.Logf("Wrong number of unparsed args (expected 2, got %d)", len(unparsed))
		t.FailNow()
	}

	if unparsed[0] != "xxx" || unparsed[1] != "zzz" {
		t.Errorf("Wrong values of unparsed args: %v", unparsed)
	}
}

func TestNumeric(t *testing.T) {
	os.Args = []string{"test.exe", "-a", "123", "-b", "-456", "-c", "3.1415"}
	resetForTests()

	var a, b int
	var c float64

	Integer("a", "", &a, "int", "integer a")
	Integer("b", "", &b, "int", "integer b")
	Float("c", "", &c, "float", "floating point c")

	err := Parse()
	if err != nil {
		t.Logf("Parse() returned an error: %s", err.Error())
		t.FailNow()
	}

	if a != 123 {
		t.Errorf("a should be 123, got %d", a)
	}

	if b != -456 {
		t.Errorf("a should be -456, got %d", b)
	}

	if c < 3.1414999 || c > 3.1415001 {
		t.Errorf("c should be 3.1415, got %1.9f", c)
	}
}
